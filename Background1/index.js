function calc() {
    let container = document.getElementById('container');
    let height = window.innerHeight;
    let width = window.innerWidth;
    if (container.innerHTML !== '') {
        var length = document.querySelectorAll('#container .square').length;
        console.log(length + "/" + (Math.floor(height / 90) * Math.floor(width / 70)));
        if ((Math.floor(height / 90) * Math.floor(width / 70)) > length) {
            while (length !== (Math.floor(height / 70) * Math.floor(width / 60))) {
                length++;
                let rotation = anime.random(-120, 120);
                let scale = anime.random(-15, 15) / 10;
                let opacity = anime.random(1, 4) / 10;
                let random_boolean = Math.random() >= 0.3;
                if(random_boolean) {
                    container.innerHTML += '<div class="square" style="transform: rotate(' + rotation + 'deg) scale(' + scale + '); opacity: ' + opacity + '"></div>';
                }else {
                    container.innerHTML += '<div class="square" style="transform: rotate(' + rotation + 'deg) scale(' + scale + '); opacity: ' + opacity + '; border-radius: 5000px;"></div>';
                }
            }
        } else if ((Math.floor(height / 90) * Math.floor(width / 70)) < length) {
            while (length !== (Math.floor(height / 90) * Math.floor(width / 70))) {
                let entries = document.querySelectorAll('#container .square');
                entries.item(entries.length - 1).remove();
                length--;
            }
        }
        animate();
        return;
    }
    let i, i2;
    for (i = 0; i < Math.floor(height / 90); i++) {
        for (i2 = 0; i2 < Math.floor(width / 70); i2++) {
            let rotation = anime.random(-120, 120);
            let scale = anime.random(-15, 15) / 10;
            let opacity = anime.random(1, 4) / 10;
            let random_boolean = Math.random() >= 0.3;
            if(random_boolean) {
                container.innerHTML += '<div class="square" style="transform: rotate(' + rotation + 'deg) scale(' + scale + '); opacity: ' + opacity + '"></div>';
            }else {
                container.innerHTML += '<div class="square" style="transform: rotate(' + rotation + 'deg) scale(' + scale + '); opacity: ' + opacity + '; border-radius: 5000px;"></div>';
            }
        }
    }
}

function animate() {
    anime({
        targets: '.square',
        rotate: function () {
            return anime.random(-120, 120);
        },
        scale: function () {
            return anime.random(-12, 12) / 10;
        },
        opacity: function () {
            return anime.random(0, 4) / 10;
        },
        duration: 45000
    });
}